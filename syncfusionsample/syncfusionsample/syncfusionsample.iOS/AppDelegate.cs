﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Prism.Unity;
using Microsoft.Practices.Unity;
using Syncfusion.ListView.XForms.iOS;
using Interface;
using SqliteRepostory;
using Syncfusion.SfDataGrid.XForms.iOS;

namespace syncfusionsample.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            var container = new XLabs.Ioc.Unity.UnityDependencyContainer();

            var conn = new DbImplement().DbConnect();

            container.Register<IActorRepository>(new ActorRepostory(conn));
            container.Register<IOrderInfoRepostory>(new OrderInfoRepostory(conn));

            XLabs.Ioc.Resolver.SetResolver(container.GetResolver());

            global::Xamarin.Forms.Forms.Init();
            SfListViewRenderer.Init();
            SfDataGridRenderer.Init();


            LoadApplication(new App(new iOSInitializer()));

            return base.FinishedLaunching(app, options);
        }
    }

    public class iOSInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IUnityContainer container)
        {

        }
    }

}
