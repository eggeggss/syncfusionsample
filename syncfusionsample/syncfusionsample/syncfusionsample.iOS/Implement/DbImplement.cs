﻿using syncfusionsample.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using syncfusionsample.iOS;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(DbImplement))]
namespace syncfusionsample.iOS
{
    public class DbImplement : IDBConnect
    {
        public SQLiteConnection DbConnect()
        {
            var dbName = "myDB.db3";
            string personalFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, dbName);
            return new SQLiteConnection(path);
        }
    }
}