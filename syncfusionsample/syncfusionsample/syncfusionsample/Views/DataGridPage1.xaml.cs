﻿using Xamarin.Forms;

namespace syncfusionsample.Views
{
    public partial class DataGridPage1 : ContentPage
    {
        public DataGridPage1()
        {
            InitializeComponent();

            
            //this.dataGrid.QueryRowDragging += DataGrid_QueryRowDragging;
        }

        private void DataGrid_QueryRowDragging(object sender, Syncfusion.SfDataGrid.XForms.QueryRowDraggingEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine($"From:{e.From}", "DEbug");
            System.Diagnostics.Debug.WriteLine($"To:{e.To}", "DEbug");

        }
    }
}
