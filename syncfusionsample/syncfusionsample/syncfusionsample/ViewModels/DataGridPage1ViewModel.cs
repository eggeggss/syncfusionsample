﻿using Interface;
using Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace syncfusionsample.ViewModels
{
    
    public class ItemTappedEventArgsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemTappedEventArgs = value as Syncfusion.SfDataGrid.XForms.QueryRowDraggingEventArgs;
            if (itemTappedEventArgs == null)
            {
                throw new ArgumentException("Expected value to be of type ItemTappedEventArgs", nameof(value));
            }
            return itemTappedEventArgs;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    
    public class DataGridPage1ViewModel : BindableBase, INavigationAware
    {
        #region Repositories (遠端或本地資料存取)

        #endregion

        


        #region OrderInfos
        private ObservableCollection<OrderInfo> _orders;
        /// <summary>
        /// PropertyDescription
        /// </summary>
        public ObservableCollection<OrderInfo> OrderInfos
        {
            get { return this._orders; }
            set { this.SetProperty(ref this._orders, value); }
        }
        #endregion


        // public DelegateCommand QueryRowDraggingCommand { get; set; }

        public ObservableCollection<OrderInfo> clonelist = new ObservableCollection<OrderInfo>();

        public DelegateCommand<object> QueryRowDraggingCommand { get; set; }
    
        public DelegateCommand PrintCommand { set; get; }
        
        private readonly INavigationService _navigationService;

        private readonly IOrderInfoRepostory _orderinfo;
        
        #region Constructor 建構式
        public DataGridPage1ViewModel(INavigationService navigationService)
        {

            #region 相依性服務注入的物件

            _navigationService = navigationService;
            #endregion

            this._orderinfo = XLabs.Ioc.Resolver.Resolve<IOrderInfoRepostory>();

            this.OrderInfos = new ObservableCollection<OrderInfo>(this._orderinfo.GetAll());

            foreach (var item in OrderInfos)
            {
                var obj=item.Clone();

                if(obj!=null)
                  this.clonelist.Add(obj);
            }
            

            this.QueryRowDraggingCommand = new DelegateCommand<object>((e) =>
            {
               
                var evenarg = (Syncfusion.SfDataGrid.XForms.QueryRowDraggingEventArgs)e;

                if (evenarg.Reason == Syncfusion.SfDataGrid.XForms.QueryRowDraggingReason.DragEnded)
                {
                    int old1 = evenarg.From;
                    int new1 = evenarg.To; 
                    System.Diagnostics.Debug.WriteLine($"From:{old1}", "debug");
                    System.Diagnostics.Debug.WriteLine($"To:{new1}", "debug");

                    if (old1 < new1)
                    {
                        old1 = old1 - 1;
                        new1 = new1 - 2;
                        clonelist.RemoveAt(old1);
                        clonelist.Insert(new1, (OrderInfo)evenarg.RowData);
                    }


                }

            });

            this.PrintCommand = new DelegateCommand(() => 
            {
                //String.Join(",",this.OrderInfos.ToArray)

                

                foreach (var item in this.clonelist)
                {
                    System.Diagnostics.Debug.WriteLine(item.OrderID.ToString(), "debug");
                }

                //var Orderid=this.OrderInfos.ToList().Select((e) => { return e.OrderID; }).ToArray();

                //System.Diagnostics.Debug.WriteLine( String.Join(",", Orderid),"debug");
            });

        }

        #endregion

        #region Navigation Events (頁面導航事件)
        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public async void OnNavigatedTo(NavigationParameters parameters)
        {
            await ViewModelInit();
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
        #endregion

        #region 設計時期或者執行時期的ViewModel初始化
        #endregion

        #region 相關事件
        #endregion

        #region 相關的Command定義
        #endregion

        #region 其他方法

        /// <summary>
        /// ViewModel 資料初始化
        /// </summary>
        /// <returns></returns>
        private async Task ViewModelInit()
        {
            await Task.Delay(100);
        }
        #endregion

    }

}
