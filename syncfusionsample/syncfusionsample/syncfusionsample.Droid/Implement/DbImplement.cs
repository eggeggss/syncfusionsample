﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using syncfusionsample.Droid;
using syncfusionsample.Interface;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(DbImplement))]
namespace syncfusionsample.Droid
{
    public class DbImplement : IDBConnect
    {
        public SQLiteConnection DbConnect()
        {
            var dbName = "myDB.db3";
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);

            return new SQLite.SQLiteConnection(path);

        }
    }
}