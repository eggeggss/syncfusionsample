﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Prism.Unity;
using Microsoft.Practices.Unity;
using Interface;
using SqliteRepostory;

namespace syncfusionsample.Droid
{
    [Activity(Label = "syncfusionsample", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.tabs;
            ToolbarResource = Resource.Layout.toolbar;

            var container = new XLabs.Ioc.Unity.UnityDependencyContainer();

            DbImplement db = new DbImplement();
            var conn=db.DbConnect();

            container.Register<IActorRepository>(new ActorRepostory(conn));
            container.Register<IOrderInfoRepostory>(new OrderInfoRepostory(conn));
            XLabs.Ioc.Resolver.SetResolver(container.GetResolver());

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App(new AndroidInitializer()));
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IUnityContainer container)
        {

        }
    }
}

