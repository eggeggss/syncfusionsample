﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class Actor
    {
        [PrimaryKey, AutoIncrement]
        public int pk { set; get; }

        public String Name { set; get; }
        public String ImageUrl { set; get; }
    }
}
