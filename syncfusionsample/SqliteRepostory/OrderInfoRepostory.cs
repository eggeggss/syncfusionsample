﻿using Interface;
using Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqliteRepostory
{
    public class OrderInfoRepostory:MySqliteRespostory,IOrderInfoRepostory
    {
        private ObservableCollection<OrderInfo> orderInfo;
        public ObservableCollection<OrderInfo> OrderInfoCollection
        {
            get { return orderInfo; }
            set { this.orderInfo = value; }
        }

        public OrderInfoRepostory(SQLiteConnection conn):base(conn)
        {
            this._connTmp.CreateTable<OrderInfo>();
            orderInfo = new ObservableCollection<OrderInfo>();
            this.GenerateOrders();
        }

        private void GenerateOrders()
        {
            orderInfo.Add(new OrderInfo(1001, "Maria Anders", "Germany", "ALFKI", "Berlin"));
            orderInfo.Add(new OrderInfo(1002, "Ana Trujillo", "Mexico", "ANATR", "Mexico D.F."));
            orderInfo.Add(new OrderInfo(1003, "Ant Fuller", "Mexico", "ANTON", "Mexico D.F."));
            orderInfo.Add(new OrderInfo(1004, "Thomas Hardy", "UK", "AROUT", "London"));
            orderInfo.Add(new OrderInfo(1005, "Tim Adams", "Sweden", "BERGS", "London"));
            orderInfo.Add(new OrderInfo(1006, "Hanna Moos", "Germany", "BLAUS", "Mannheim"));
            orderInfo.Add(new OrderInfo(1007, "Andrew Fuller", "France", "BLONP", "Strasbourg"));
            orderInfo.Add(new OrderInfo(1008, "Martin King", "Spain", "BOLID", "Madrid"));
            orderInfo.Add(new OrderInfo(1009, "Lenny Lin", "France", "BONAP", "Marsiella"));
            orderInfo.Add(new OrderInfo(1010, "John Carter", "Canada", "BOTTM", "Lenny Lin"));
            orderInfo.Add(new OrderInfo(1011, "Laura King", "UK", "AROUT", "London"));
            orderInfo.Add(new OrderInfo(1012, "Anne Wilson", "Germany", "BLAUS", "Mannheim"));
            orderInfo.Add(new OrderInfo(1013, "Martin King", "France", "BLONP", "Strasbourg"));
            orderInfo.Add(new OrderInfo(1014, "Gina Irene", "UK", "AROUT", "London"));

            this.DeleteAll();
            this.InsertAll(orderInfo);
        }

        public OrderInfo Get(int pk)
        {
            return this.GetAll().FirstOrDefault(e => e.OrderID == pk);
        }

        public IList<OrderInfo> GetAll()
        {
            return this._connTmp.Table<OrderInfo>().ToList();
        }

        public void Insert(OrderInfo obj)
        {
            this._connTmp.Insert(obj);
        }

        public void InsertAll(IList<OrderInfo> lists)
        {
            this._connTmp.InsertAll(lists);
        }

        public void Delete(OrderInfo obj)
        {
            this._connTmp.Delete<OrderInfo>(obj);
        }

        public void DeleteAll()
        {
            this._connTmp.DeleteAll<OrderInfo>();
        }

        public void Update(OrderInfo obj)
        {
            this._connTmp.Update(obj);
        }

        public void UpdateAll(IList<OrderInfo> lists)
        {
            this._connTmp.UpdateAll(lists);
        }
    }
}
