﻿using Interface;
using Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqliteRepostory
{
    public class ActorRepostory : MySqliteRespostory, IActorRepository
    {

        

        public ActorRepostory(SQLiteConnection conn):base(conn)
        {
            
            conn.CreateTable<Actor>();
        }


        public void Delete(Actor obj)
        {
            this._connTmp.Delete(obj);
        }

        public void DeleteAll()
        {
            this._connTmp.DeleteAll<Actor>();
        }

        public Actor Get(string name)
        {
           return _connTmp.Table<Actor>().FirstOrDefault((e) => { return e.Name==name; });
        }

        public Actor Get(int pk)
        {
            return null;
        }

        public IList<Actor> GetAll()
        {
            return _connTmp.Table<Actor>().AsEnumerable().ToList();
        }

        public void InsertAll(IList<Actor> lists)
        {
            _connTmp.InsertAll(lists);
        }

        public void Insert(Actor obj)
        {
            _connTmp.Insert(obj);
        }

        public void Update(Actor obj)
        {
            _connTmp.Update(obj);
        }

        public void UpdateAll(IList<Actor> lists)
        {
            _connTmp.UpdateAll(lists);
        }
    }
}
