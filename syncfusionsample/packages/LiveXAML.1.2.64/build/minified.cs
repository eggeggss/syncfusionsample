/* Copyright (C) Ion OÜ - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Mihhail Maslakov <mihhail.maslakov@gmail.com>, 2017
 */
#pragma warning disable
namespace __livexaml { 
using System;using System.Collections.Generic;using System.Diagnostics;using
System.Linq;using System.Reflection;using System.Text;using System.Threading.
Tasks;using Xamarin.Forms;using Xamarin.Forms.Xaml;using Xamarin.Forms.Xaml.
Internals;using System.Collections.Specialized;using System.Xml.Linq;using
Xamarin.Forms.Internals;using System.Threading;using System.IO;using System.Xml;
internal class a1{private string[]x;private int y;public a1(string[]a){if(a.
Length==0)throw new InvalidOperationException(
"Can't initialize circular buffer with an empty collection");x=a;}public string
Get(){return x[y++%x.Length];}}internal class a2{public static void WriteLine(
string a){Debug.WriteLine("XL: "+a);}public static void Exception(Exception a){
if(a is TargetInvocationException){var b=(TargetInvocationException)a;Debug.
WriteLine("XL: "+b.InnerException.Message);}else{Debug.WriteLine("XL: "+a.
Message);}}internal static void x(string a){Debug.WriteLine("XL: "+a);}}internal
static class a3{private static readonly Dictionary<string,Type>x=new Dictionary<
string,Type>();private static readonly Assembly[]y;static a3(){try{y=B();}catch{
a2.x("Failed to load assemblies");y=new Assembly[]{typeof(Element).GetTypeInfo()
.Assembly,typeof(string).GetTypeInfo().Assembly};}}public static Tuple<Type,
FieldInfo>FindShortTypeWithProperty(string a,string b){z();foreach(var kvp in x)
{if(kvp.Key.EndsWith("."+a)){var c=FindBindablePropertyField(kvp.Value,b);if(c!=
null)return Tuple.Create(kvp.Value,c);}}return null;}public static Type FindType
(string a){z();Type b;if(x.TryGetValue(a,out b))return b;return A(a);}private
static void z(){if(x.Count==0){var a=y.Where(b=>b!=null).SelectMany(b=>b.
DefinedTypes);foreach(var type in a)x[type.FullName]=type.AsType();}}private
static Type A(string a){var b=new[]{
"System.Net.Sockets, Version=4.1.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
,
"System.Net.Sockets, Version=4.3.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
,
"System.Net.Primitives, Version=4.0.11.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
,
"System.Net.Primitives, Version=4.3.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
,
"System.Net.Primitives, Version=4.0.10.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
,};if(string.IsNullOrWhiteSpace(a))throw new InvalidOperationException(
"Type name shouldn't be empty or null");if(a.StartsWith("System.Net",
StringComparison.OrdinalIgnoreCase)){foreach(var netAssembly in b){try{var c=
Type.GetType(a+", "+netAssembly);if(c!=null)return c;}catch{}}}return null;}
public static FieldInfo FindBindablePropertyField(Type a,string b){var c=a.
GetTypeInfo();foreach(var declaredField in c.DeclaredFields)if(declaredField.
IsStatic&&declaredField.Name==b+"Property")return declaredField;if(c.BaseType==
null)return null;return FindBindablePropertyField(c.BaseType,b);}public static
ConstructorInfo GetConstructor(Type a,params Type[]b){var c=a.GetConstructors().
FirstOrDefault(d=>{var f=d.GetParameters();var g=f.Length;return g==b.Length&&f.
Zip(b,(h,i)=>h.ParameterType.IsAssignableFrom(i)).All(j=>j);});if(c==null)throw
new InvalidOperationException("Couldn't find constructor for "+a+
" with parameters: "+string.Join(", ",b.Select(k=>k.Name)));return c;}private
static Assembly[]B(){var a=typeof(string).GetTypeInfo();var b=a.Assembly;var c=b
.GetType("System.AppDomain");var d=c.GetRuntimeProperty("CurrentDomain");var f=d
.GetMethod;var g=f.Invoke(null,new object[]{});var h=g.GetType().
GetRuntimeMethod("GetAssemblies",new Type[]{});return h.Invoke(g,new object[]{})
as Assembly[];}}internal class a4{private static Lazy<a4>x=new Lazy<a4>(()=>new
a4());public static a4 Instance{get{return x.Value;}}private readonly a5 y=new
a5();private readonly byte[]z=new byte[1024*100];private readonly ac A;private
const int B=53030;private const int C=53050;private string D;private ah E;
private string F="10.211.55.4";private a4(){y.MessageReceived+=M;if(Device.OS==
TargetPlatform.Android){A=new ac(new[]{F,"169.254.80.80","10.0.2.2","10.0.3.2"},
H,G);}else if(Device.OS==TargetPlatform.iOS){A=new ac(new[]{F,"127.0.0.1",
"169.254.80.80","10.0.2.2","10.0.3.2"},H,G);}else{A=new ac(new[]{F,"127.0.0.1",
"169.254.80.80","10.0.2.2","10.0.3.2"},H,G);}}private void G(){a2.x(
"Failed to find find a handshake server");a2.x("Trying to use UDP connection");
var a=new aj(C);a.BeginReceive(K,a);}private void H(string a){D=a;I(D);}private
void I(string a){var b=new ai();try{a2.x("Listener connecting to "+a);b.B(a,B).
Wait(20);a2.x("Listener connected to "+a);E=b.Client;J(E,0);}catch(Exception e){
a2.x("Listener failed to connect to "+a);a2.Exception(e);}}private void J(ah a,
int b){try{if(b>=z.Length){b=0;}a.BeginReceive(z,b,z.Length-b,0,L,b);}catch(
Exception e){a2.WriteLine("BeginReceive failed");a2.Exception(e);I(D);}}private
void K(IAsyncResult a){aj b=null;try{b=(aj)a.AsyncState;var c=new af(ae.Any,C).
Object;var d=b.EndReceive(a,ref c);y.Feed(d,d.Length);b.BeginReceive(K,b);}catch
(Exception e){a2.WriteLine("UdpEndReceive failed");a2.Exception(e);if(b!=null)((
IDisposable)b).Dispose();b=new aj(C);b.BeginReceive(K,b);}}private void L(
IAsyncResult a){try{var b=(int)a.AsyncState;var c=E.EndReceive(a);a2.WriteLine(
"Listener EndReceive ("+c+")");if(c==0){a2.WriteLine(
"Connection to the server has ended");I(D);return;}y.Feed(z,c);J(E,0);}catch(
Exception e){a2.WriteLine("EndReceive failed");a2.Exception(e);I(D);}}private
void M(object a,a7 b){a2.WriteLine("Listener ParserOnMessageReceived");var c=b.
Messages;a8.Run(()=>{try{ab.ReceiveMessages(c);}catch(Exception ex){a2.WriteLine
("ReceiveMessages failed");a2.Exception(ex);}});}}internal class a5{public event
EventHandler<a7>MessageReceived;private readonly List<byte>x=new List<byte>();
private int y=0;private int z;private string A;private int B;private byte[]C;
private ushort D;private byte E;private List<Message>F;private ushort G;private
string H;private ushort I;private DateTime J=DateTime.Now;public void Feed(byte[
]a,int b){if((DateTime.Now-J).TotalMilliseconds>1000)y=0;J=DateTime.Now;for(int
c=0;c<b;c++)K(a[c]);}private void K(byte a){switch(y){case 0:if(a==0xbe){F=new
List<Message>();y=1;}break;case 1:if(a==0xef){y=2;}break;case 2:E=a;y=3;break;
case 3:x.Add(a);if(x.Count==4){z=BitConverter.ToInt32(x.ToArray(),0);x.Clear();
if(z<=0||z>10000)y=0;else y=4;}break;case 4:x.Add(a);if(x.Count==z){A=Encoding.
Unicode.GetString(x.ToArray(),0,x.Count);x.Clear();y=5;}break;case 5:x.Add(a);if
(x.Count==4){B=BitConverter.ToInt32(x.Take(4).ToArray(),0);x.Clear();y=6;if(B<=0
||B>1024*1024){y=0;}else{y=6;}}break;case 6:x.Add(a);if(x.Count==B){var b=x.
ToArray();C=b;D=Fletcher16(b);x.Clear();y=7;}break;case 7:x.Add(a);if(x.Count==2
){var c=BitConverter.ToUInt16(x.ToArray(),0);x.Clear();if(D==c){F.Add(new
Message{TargetId=A,Buffer=C});y=8;}else y=0;}break;case 8:x.Add(a);if(x.Count==2
){G=BitConverter.ToUInt16(x.ToArray(),0);x.Clear();if(G>0)y=9;else if(E>1){E--;y
=3;}else y=10;}break;case 9:x.Add(a);if(x.Count==G){F.Last().PropertyList=
Encoding.Unicode.GetString(x.ToArray(),0,x.Count);x.Clear();if(--E>0)y=3;else y=
10;}break;case 10:if(a==0xff){var d=MessageReceived;if(d!=null){var f=H==A&&I==D
;I=D;H=A;if(!f)d(this,new a7{Messages=F});}}y=0;break;default:throw new
ArgumentOutOfRangeException();}}public ushort Fletcher16(byte[]a){ushort b=0;
ushort c=0;for(var d=0;d<a.Length;++d){b=(ushort)((b+a[d])%255);c=(ushort)((c+b)
%255);}return(ushort)((c<<8)|b);}}public class Message{public string TargetId{
get;set;}public byte[]Buffer{get;set;}public string PropertyList{get;set;}
internal ab.XamlBuffer x{get;set;}}internal class a7:EventArgs{public List<
Message>Messages{get;set;}}internal class a8{public static void Run(Action a){
Device.BeginInvokeOnMainThread(a);}}internal static class a9{private static
readonly HashSet<Type>x=new HashSet<Type>{typeof(int),typeof(double),typeof(
decimal),typeof(long),typeof(short),typeof(sbyte),typeof(byte),typeof(ulong),
typeof(ushort),typeof(uint),typeof(float)};private static string[]y;public
static string[]GetTypeNames(){if(y==null)y=x.Select(a=>a.FullName).ToArray();
return y;}public static bool TypeIsNumeric(Type a){return x.Contains(Nullable.
GetUnderlyingType(a)??a);}public static bool TypeIsNumeric(string a){return x.
Any(b=>b.FullName==a);}}internal static class aa{public static IEnumerable<
MethodInfo>GetAllDeclaredMethods(this Type a){foreach(var method in a.
GetTypeInfo().DeclaredMethods)yield return method;var b=a.GetTypeInfo().BaseType
;if(b==null)yield break;foreach(var baseTypeMethod in GetAllDeclaredMethods(b))
yield return baseTypeMethod;}public static MethodInfo GetMethod(this Type a,
string b,bool c,Type[]d=null){var f=a.GetTypeInfo().GetDeclaredMethods(b).
FirstOrDefault(g=>(c?!g.IsStatic:g.IsStatic)&&x(g,d));if(f==null)throw new
Exception("Method `"+b+"` on type `"+a.Name+"` not found");return f;}public
static ConstructorInfo[]GetConstructors(this Type a){return a.GetTypeInfo().
DeclaredConstructors.ToArray();}public static ConstructorInfo FindConstructor(
this Type a,params Type[]b){return a.GetTypeInfo().DeclaredConstructors.
FirstOrDefault(c=>{var d=c.GetParameters();if(d.Length!=b.Length)return false;
return d.Zip(b,(f,g)=>f.ParameterType==g).All(h=>h);});}public static bool
IsAssignableFrom(this Type a,Type b){return a.GetTypeInfo().IsAssignableFrom(b.
GetTypeInfo());}private static bool x(MethodInfo a,Type[]b){if(b==null)return
true;var c=a.GetParameters();if(c.Length!=b.Length)return false;for(var d=0;d<b.
Length;d++)if(!c[d].ParameterType.IsAssignableFrom(b[d]))return false;return
true;}}internal static class Runtime{public static readonly BindableProperty
RegisterProperty=BindableProperty.CreateAttached("Register",typeof(string),
typeof(Runtime),"",BindingMode.OneWay,null,RegisterPropertyChanged);private
static a4 _listenerInstance;private static void RegisterPropertyChanged(
BindableObject a,object b,object c){try{var d=Device.OS==TargetPlatform.Android
||Device.OS==TargetPlatform.iOS||Device.OS==TargetPlatform.Windows||Device.OS==
TargetPlatform.WinPhone;if(d&&_listenerInstance==null)_listenerInstance=a4.
Instance;}catch(Exception e){a2.WriteLine(e.ToString());throw;}var f=(string)c;
if(!f.Equals(ab.CurrentlyUpdatedTargetId,StringComparison.OrdinalIgnoreCase)){
Device.BeginInvokeOnMainThread(()=>{LoadComponent(a,f);});}Device.
BeginInvokeOnMainThread(()=>{ab.Register(a,f);});}public static void SetRegister
(BindableObject a,string b){a.SetValue(RegisterProperty,b);}public static string
GetRegister(BindableObject a){return(string)a.GetValue(RegisterProperty);}
private static readonly Queue<Tuple<Element,string>>x=new Queue<Tuple<Element,
string>>();private static void y(object a,string b){foreach(var value in b.Split
(','))LoadComponent(a,value);}public static bool LoadComponent(object a,string b
){try{var c=ab.GetInitialPropertyList(b)??"";var d=ab.FindBuffer(b);if(d!=null){
ab.ClearElement(a,b,c);if(!(a is Cell))ab.InitializeComponent(a,d);return true;}
else if(XamlLoader.XamlFileProvider!=null){var f=XamlLoader.XamlFileProvider(a.
GetType())!=null;a2.WriteLine(b+" has xaml resource: "+f);if(f){ab.ClearElement(
a,b,c);a.LoadFromXaml(a.GetType());return true;}}return false;}catch(
NullReferenceException){}catch(Exception e){a2.WriteLine(
"Failed to initialize with: "+b);a2.WriteLine(e.Message);}return false;}}
internal class ab{public static string CurrentlyUpdatedTargetId{get;private set;
}static readonly HashSet<object>x=new HashSet<object>();static readonly
Dictionary<object,object>y=new Dictionary<object,object>();static readonly
Dictionary<object,string>z=new Dictionary<object,string>();static readonly
Dictionary<string,HashSet<object>>A=new Dictionary<string,HashSet<object>>();
static readonly Dictionary<string,XamlBuffer>B=new Dictionary<string,XamlBuffer>
();static readonly Dictionary<string,string>C=new Dictionary<string,string>();
public static void Register(object a,string b){var c=a as Element;b=b.ToLower();
HashSet<object>d;if(!A.TryGetValue(b,out d))A[b]=d=new HashSet<object>();d.Add(a
);z[a]=b;x.Add(a);D(a);a2.WriteLine("Registered '"+b+"' as "+a);}private static
void D(object a){var b=a as Element;if(b==null)return;var c=b;var d=b.Parent;
while(d!=null){if(y.ContainsKey(c))break;y[c]=d;c=d;d=c.Parent;}}public static
XamlBuffer FindBuffer(string a){a=a.ToLower();XamlBuffer b;if(B.TryGetValue(a,
out b))return b;return null;}public static void ReceiveMessages(List<Message>a){
foreach(var message in a){message.TargetId=message.TargetId.ToLower();message.x=
B[message.TargetId]=a0(message.Buffer);}var b=new List<HashSet<object>>();
foreach(var message in a){HashSet<object>c;if(A.TryGetValue(message.TargetId,out
c)){b.Add(c);if(message.x.CellXamlString==null){foreach(var obj in c.OfType<Cell
>()){var d=message.x;d.CellXamlString=ad.TransformXaml(message.x.XamlString,new
List<string>(),message.TargetId);}}}}var f=Y(b.SelectMany(g=>g).ToList());
foreach(var root in f.Where(h=>!(h is Cell))){var i=root;string j;if(!z.
TryGetValue(i,out j)){a2.WriteLine(
"ReceiveMessages couldn't find in ObjectToTarget");continue;}var k=a.
FirstOrDefault(l=>l.TargetId==j);if(k==null){a2.WriteLine(
"ReceiveMessages page message for "+j+" not found");continue;}a2.WriteLine(
"Updating '"+k.TargetId+"' as "+i);if(i!=Application.Current)U(i);
CurrentlyUpdatedTargetId=k.TargetId;ClearElement(i,k.TargetId,k.PropertyList);
InitializeComponent(i,k.x);}E(f);}private static void E(HashSet<object>a){var b=
a.OfType<Cell>().FirstOrDefault();if(b==null)return;var c=a.OfType<Cell>().
Select(d=>d.Parent).OfType<ListView>().Distinct().ToList();string f=null;foreach
(var root in a.OfType<Cell>()){z.TryGetValue(root,out f);V(root);}var g=b.
GetType();XamlBuffer h;if(!B.TryGetValue(f,out h))return;var i=XamlLoader.
XamlFileProvider;Func<Type,string>j=k=>{if(k==g)return h.CellXamlString;if(i==
null)return null;return i(k);};G(j);foreach(var parentListView in c){if(
parentListView!=null&&f!=null){var l=parentListView.ItemsSource;parentListView.
ItemsSource=null;if(!(parentListView.ItemTemplate is DataTemplateSelector)){
parentListView.ItemTemplate=new DataTemplate(()=>{var m=Activator.CreateInstance
(b.GetType());NameScope.SetNameScope((Cell)m,new NameScope());return m;});}a8.
Run(()=>{parentListView.ItemsSource=l;});}}}public static void
InitializeComponent(object a,XamlBuffer b){try{a2.WriteLine(
"InitializeComponent "+a);var c=N(a.GetType()).ToArray();K(a,c);if(!H(a,b.
XamlString)){a2.WriteLine("Constructor initializing failed. Reloading XAML.");Q(
a,b.XamlString);P(a,b.XNames);}L(a,c);M(a);R(a);if(a is Application)S((
Application)a);}catch(TargetInvocationException e){T(a,e.InnerException);}catch(
Exception e){T(a,e);}}static PropertyInfo F=typeof(XamlLoader).
GetRuntimeProperty("XamlFileProvider");private static void G(Func<Type,string>a)
{F.SetValue(null,a);}private static bool H(object a,string b){try{var c=a.
GetType();var d=c.GetConstructors().FirstOrDefault(f=>f.GetParameters().Length==
0);if(d==null)return false;var g=XamlLoader.XamlFileProvider;Func<Type,string>h=
i=>{if(i==c)return b;if(g==null)return null;return g(i);};G(h);var j=Activator.
CreateInstance(c);G(g);Q(a,b);var k=O(c);if(k==null)return false;var l=c.
GetRuntimeProperties().FirstOrDefault(m=>m.Name==k);var n=l.GetValue(a);if(a is
Grid){I((Grid)j,(Grid)a);}else if(n is System.Collections.IEnumerable){var o=n.
GetType().GetRuntimeMethods();var p=o.FirstOrDefault(q=>q.Name=="Clear");var r=o
.FirstOrDefault(q=>q.Name=="Add");if(p==null||r==null)return false;var s=l.
GetValue(j)as System.Collections.IEnumerable;if(s==null)return false;p.Invoke(n,
new object[0]);foreach(var newValue in s)r.Invoke(n,new object[]{newValue});var
t=c.GetAllDeclaredMethods().ToList();var u=t.FirstOrDefault(q=>q.Name==
"OnChildrenChanged");if(u!=null){u.Invoke(a,new object[]{a,new
NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)});}var v=t
.FirstOrDefault(q=>q.Name=="OnPagesChanged");if(v!=null){v.Invoke(a,new object[]
{new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)});}}
else{l.SetValue(a,l.GetValue(j));}J(a,j);var w=c.GetRuntimeFields();foreach(var
fld in w){try{if(!fld.IsLiteral&&!fld.IsStatic)fld.SetValue(a,fld.GetValue(j));}
catch(Exception){a2.WriteLine("Couldn't copy field "+fld.Name);}}return true;}
catch(Exception e){a2.WriteLine("Failed to call ctor: "+e.Message);return false;
}}private static void I(Grid a,Grid b){var c=a.Children.ToArray();b.Children.
Clear();a.Children.Clear();foreach(var child in c){var d=Grid.GetRow(child);var
f=Grid.GetRowSpan(child);var g=Grid.GetColumn(child);var h=Grid.GetColumnSpan(
child);b.Children.Add(child,g,g+h,d,d+f);}}private static void J(object a,object
b){if(a is Page&&b is Page){((Page)a).ToolbarItems.Clear();foreach(var newItem
in((Page)b).ToolbarItems)((Page)a).ToolbarItems.Add(newItem);}}private static
void K(object a,FieldInfo[]b){var c=a.GetType();var d=b.FirstOrDefault(f=>f.Name
=="Disappearing");var g=c.GetTypeInfo().DeclaredMethods.FirstOrDefault(h=>h.Name
=="OnDisappearing"&&h.GetParameters().Length==0);if(g!=null)g.Invoke(a,null);if(
d!=null){var i=(EventHandler)d.GetValue(a);if(i!=null)i.Invoke(a,EventArgs.Empty
);}}private static void L(object a,FieldInfo[]b){var c=a.GetType();var d=b.
FirstOrDefault(f=>f.Name=="Appearing");var g=c.GetTypeInfo().DeclaredMethods.
FirstOrDefault(h=>h.Name=="OnAppearing"&&h.GetParameters().Length==0);if(g!=null
)g.Invoke(a,null);if(d!=null){var i=(EventHandler)d.GetValue(a);if(i!=null)i.
Invoke(a,EventArgs.Empty);}}private static void M(object a){try{var b=a as
Xamarin.Forms.BindableObject;if(b==null)return;var c=b.BindingContext;if(c==null
)return;b.BindingContext=null;b.BindingContext=c;}catch(Exception e){a2.
WriteLine("Failed to refresh BindingContext: "+e.Message);}}private static
IEnumerable<FieldInfo>N(Type a){foreach(var fld in a.GetRuntimeFields())yield
return fld;var b=a.GetTypeInfo().BaseType;if(b!=null){foreach(var baseTypeFld in
N(b)){yield return baseTypeFld;}}}private static string O(Type a){TypeInfo b=a.
GetTypeInfo();var c=b.CustomAttributes.FirstOrDefault(d=>d.AttributeType==typeof
(ContentPropertyAttribute));if(c!=null&&c.ConstructorArguments.Count>0){var f=c.
ConstructorArguments[0];if(f.ArgumentType==typeof(string))return(string)f.Value;
}if(b.BaseType!=null)return O(b.BaseType);return null;}private static void P(
object a,IReadOnlyList<string>b){var c=typeof(NameScopeExtensions).
GetRuntimeMethod("FindByName",new[]{typeof(Element),typeof(string)});var d=a.
GetType();var f=d.GetRuntimeFields();foreach(var xname in b){try{var g=f.
FirstOrDefault(h=>h.Name==xname);if(g!=null){var i=c.MakeGenericMethod(g.
FieldType);var j=i.Invoke(null,new object[]{a,xname});g.SetValue(a,j);}else{a2.
WriteLine("Couldn't find field "+xname+" on type "+d.Name);}}catch(Exception e){
a2.WriteLine("Failed to assign named control "+xname);a2.WriteLine(e.ToString())
;}}}private static void Q(object a,string b){var c=typeof(Xamarin.Forms.Xaml.
Extensions).GetTypeInfo();var d=c.DeclaredMethods.FirstOrDefault(f=>{if(f.Name!=
"LoadFromXaml")return false;var g=f.GetParameters();return g.Length==2&&g[1].
ParameterType==typeof(string);});if(d==null)throw new InvalidOperationException(
"Couldn't find LoadFromXaml extension method");var h=d.MakeGenericMethod(typeof(
object));h.Invoke(null,new[]{a,b});}private static void R(object a){var b=a.
GetType().GetRuntimeMethods().Where(c=>c.Name.EndsWith("LiveXaml",
StringComparison.OrdinalIgnoreCase));foreach(var initMethod in b){if(initMethod.
GetParameters().Length>0)a2.WriteLine(
"LiveXaml init method cannot contain any parameters ("+initMethod.Name+")");else
initMethod.Invoke(a,null);}}private static void S(Application a){string b;if(z.
TryGetValue(a.MainPage,out b)){CurrentlyUpdatedTargetId=b;if(!Runtime.
LoadComponent(a.MainPage,b)){try{a.MainPage=(Page)Activator.CreateInstance(a.
MainPage.GetType());}catch(Exception e){a2.WriteLine(
"Failed to create instance of MainPage");a2.WriteLine(e.ToString());}}}}private
static void T(object a,Exception b){var c=b.Message;var d=b.InnerException;while
(d!=null){c+=Environment.NewLine+d.Message;d=d.InnerException;}var f=new Label{
Text=c,TextColor=Color.Red};if(a is ContentPage){((ContentPage)a).Content=f;}
else if(a is ContentView){((ContentView)a).Content=f;}a2.WriteLine(c);}private
static void U(object a){var b=a as Element;if(b!=null)foreach(var child in _(b))
V(child);}private static void V(Element a){if(x.Contains(a)){string b;if(z.
TryGetValue(a,out b)){HashSet<object>c;if(A.TryGetValue(b,out c))c.Remove(a);z.
Remove(a);y.Remove(a);}x.Remove(a);}}public static void ClearElement(object a,
string b,string c){ClearChildren(a);var d=a as Element;if(d!=null){var f=
NameScope.GetNameScope(d)as NameScope;if(f!=null){}}if(c!=null)W(a,b,c);}public
static void ClearChildren(object a){try{if(a is ContentPage){((ContentPage)a).
Content=null;}else if(a is ContentView){((ContentView)a).Content=null;}else if(a
is ScrollView){((ScrollView)a).Content=null;}else if(a is ContentPresenter){((
ContentPresenter)a).Content=null;}else if(a is Frame){((Frame)a).Content=null;}
else if(a is StackLayout){((StackLayout)a).Children.Clear();}else if(a is
AbsoluteLayout){((AbsoluteLayout)a).Children.Clear();}else if(a is
RelativeLayout){((RelativeLayout)a).Children.Clear();}if(a is Page){((Page)a).
ToolbarItems.Clear();}if(a is Application){var b=(Application)a;b.Resources.
Clear();}}catch(Exception e){a2.WriteLine("Unable to ClearChildren: "+e);}}
public static string GetInitialPropertyList(string a){string b;if(C.TryGetValue(
a,out b))return b;return null;}private static void W(object a,string b,string c)
{var d=a as Element;if(d==null)return;C[b]=c;foreach(var property in c.Split(','
)){if(string.IsNullOrWhiteSpace(property))continue;a2.WriteLine("ResetProperty "
+property);var f=property;var g=f.LastIndexOf('.');if(g!=-1){var h=f.Substring(0
,g);var i=f.Substring(g+1);var j=a3.FindShortTypeWithProperty(h,i);if(j==null)
continue;var k=j.Item2;var l=(BindableProperty)k.GetValue(null);d.ClearValue(l);
}else{var m=a.GetType();X(d,m,property);}}}private static void X(Element a,Type
b,string c){var d=a3.FindBindablePropertyField(b,c);if(d==null){a2.WriteLine(
"Dependency property "+c+" not found on type "+b.FullName);return;}var f=(
BindableProperty)d.GetValue(null);a.ClearValue(f);}private static HashSet<object
>Y(IList<object>a){var b=new HashSet<object>();foreach(var element in a){var c=
element as Element;if(c!=null){var d=Z(c);var f=d.LastOrDefault(g=>a.Any(h=>h!=
element&&h==g));b.Add(f??element);}else{b.Add(element);}}return b;}private
static IList<Element>Z(Element a){var b=new List<Element>();var c=a.Parent;while
(c!=null){b.Add(c);var d=c;c=d.Parent;}return b;}private static IEnumerable<
Element>_(Element a){var b=(IElementController)a;foreach(var child in b.
LogicalChildren){yield return child;foreach(var grandChild in _(child))yield
return grandChild;}}private static XamlBuffer a0(byte[]a){var b=Encoding.Unicode
.GetString(a,0,a.Length);var c=new List<string>();try{b=ad.TransformXaml(b,c);}
catch(Exception e){a2.WriteLine("Failed to transform XAML");a2.WriteLine(e.
ToString());}return new XamlBuffer(b,c);}public class XamlBuffer{public
XamlBuffer(string a,List<string>b){XamlString=a;XNames=b;}public string
XamlString{get;set;}public IReadOnlyList<string>XNames{get;set;}public string
CellXamlString{get;internal set;}}}internal class ac{private const int x=53031;
private readonly string[]y;private readonly Action z;private readonly Action<
string>A;private readonly byte[]B=new byte[1];private int C=-1;private string D;
private AutoResetEvent E=new AutoResetEvent(false);public ac(string[]a,Action<
string>b,Action c){y=a.Where(d=>!d.StartsWith("{")).ToArray();A=b;z=c;F().
ContinueWith(f=>{if(f.Exception!=null){a2.x("ServerLocator error");a2.x(f.
Exception.ToString());}});}private async Task F(){C++;if(C>y.Length-1){z();
return;}var a=y[C];try{var b=new ai();a2.x("Connecting to the handshake server "
+a);var c=b.B(a,x);var d=await Task.WhenAny(c,Task.Delay(500)).ContinueWith(f=>{
if(f.Exception!=null){a2.x("Handshake connection failed for "+a+" "+f.Exception.
Message);return false;}if(f.Result.Exception!=null){a2.x(
"Handshake connection failed for "+a+" "+f.Result.Exception.Message);return
false;}if(f.Result!=c){a2.x("Handshake connection timed out");return false;}
return true;});if(d){var g=b.Client;var h=false;var i=new
CancellationTokenSource();g.BeginReceive(B,0,B.Length,0,j=>{i.Cancel();if(!h)H(j
,g);},a);var k=Task.Delay(1000,i.Token).ContinueWith(f=>{if(f.Exception==null&&f
.Status!=TaskStatus.Canceled){h=true;E.Set();}});await G();try{if(g.Object is
IDisposable){var l=(IDisposable)g.Object;l.Dispose();}}catch{a2.x(
"Failed to properly dispose a socket for "+a);}}if(string.IsNullOrWhiteSpace(D))
{a2.WriteLine("Handshake server unreachable "+a);await F();}else{A(D);}}catch(
Exception e){a2.x("Handshake failed for "+a);a2.Exception(e);await F();}}private
Task G(){return Task.Run(()=>E.WaitOne());}private void H(IAsyncResult a,ah b){
try{var c=(string)a.AsyncState;var d=b.EndReceive(a);if(d==1&&B[0]==0xAA){D=c;E.
Set();}}catch(Exception e){a2.WriteLine("Handshake EndReceive failed");a2.
Exception(e);}}}class ad{public static string TransformXaml(string a,List<string
>b,string c=null){var d=new StringReader(a);var f=new StringWriter();using(
XmlReader g=XmlReader.Create(d)){using(XmlWriter h=XmlWriter.Create(f)){var i=
true;while(g.Read()){if(g.NodeType==XmlNodeType.Element){x(g,h,i,b,c);i=false;}
else{x(g,h,i,b,c);}}}}return f.ToString();}static void x(XmlReader a,XmlWriter b
,bool c,List<string>d,string f=null){switch(a.NodeType){case XmlNodeType.Element
:b.WriteStartElement(a.Prefix,a.LocalName,a.NamespaceURI);for(int g=0;g<a.
AttributeCount;g++){a.MoveToAttribute(g);if(c&&a.LocalName=="AutomationId")
continue;if(a.NamespaceURI=="http://schemas.microsoft.com/expression/blend/2008"
)continue;if(a.Name=="x:Name")d.Add(a.Value);b.WriteAttributeString(a.LocalName,
a.NamespaceURI,a.Value);}if(c&&f!=null)b.WriteAttributeString("Runtime.Register"
,"clr-namespace:__livexaml",f);if(a.AttributeCount>0)a.MoveToElement();if(a.
IsEmptyElement)b.WriteEndElement();break;case XmlNodeType.Text:b.WriteString(a.
Value);break;case XmlNodeType.Whitespace:case XmlNodeType.SignificantWhitespace:
break;case XmlNodeType.CDATA:b.WriteCData(a.Value);break;case XmlNodeType.
EntityReference:b.WriteEntityRef(a.Name);break;case XmlNodeType.XmlDeclaration:
case XmlNodeType.ProcessingInstruction:b.WriteProcessingInstruction(a.Name,a.
Value);break;case XmlNodeType.DocumentType:b.WriteDocType(a.Name,a.GetAttribute(
"PUBLIC"),a.GetAttribute("SYSTEM"),a.Value);break;case XmlNodeType.Comment:b.
WriteComment(a.Value);break;case XmlNodeType.EndElement:b.WriteFullEndElement();
break;default:break;}}}internal class ae{private static bool x;private static
ConstructorInfo y;public static object Any{get{if(!x)z();return y.Invoke(new
object[]{0});}}private static void z(){var a=a3.FindType("System.Net.IPAddress")
;y=a.FindConstructor(typeof(long));if(y==null)throw new Exception(
"IPAddress constructor not found");x=true;}}internal class af{public object
Object{get;}public af(object a,int b){var c=a3.FindType("System.Net.IPEndPoint")
;var d=a3.FindType("System.Net.IPAddress");var f=c.FindConstructor(d,typeof(int)
);Object=f.Invoke(new[]{a,b});}}internal class ah{public object Object{get;}
private readonly MethodInfo x;private readonly MethodInfo y;private readonly
MethodInfo z;private readonly MethodInfo A;private readonly PropertyInfo B;
private readonly PropertyInfo C;public int ReceiveTimeout{get{return(int)B.
GetValue(Object);}set{B.SetValue(Object,value);}}public bool Blocking{get{return
(bool)C.GetValue(Object);}set{C.SetValue(Object,value);}}private ah(object a){
Object=a;var b=a3.FindType("System.Net.Sockets.Socket");var c=a3.FindType(
"System.Net.Sockets.SocketFlags");z=b.GetMethod("Receive",true,new Type[]{typeof
(byte[])});x=b.GetMethod("BeginReceive",true,new Type[]{typeof(byte[]),typeof(
int),typeof(int),c,typeof(AsyncCallback),typeof(object)});y=b.GetMethod(
"EndReceive",true,new Type[]{typeof(IAsyncResult)});B=b.GetRuntimeProperty(
"ReceiveTimeout");C=b.GetRuntimeProperty("Blocking");}public static ah
FromObject(object a){return new ah(a);}public int Receive(byte[]a){return(int)z.
Invoke(Object,new[]{a});}public IAsyncResult BeginReceive(byte[]a,int b,int c,
int d,AsyncCallback f,object g){return(IAsyncResult)x.Invoke(Object,new[]{a,b,c,
(int)d,f,g});}public int EndReceive(IAsyncResult a){return(int)y.Invoke(Object,
new object[]{a});}}internal class ai{private MethodInfo x;private PropertyInfo y
;private ah z;private object A{get;}public ah Client{get{if(A==null)throw new
InvalidOperationException("Object is not initialized yet");return z??(z=ah.
FromObject(y.GetValue(A)));}}public ai(){var a=a3.FindType(
"System.Net.Sockets.TcpClient");var b=a3.GetConstructor(a);x=a.GetMethod(
"ConnectAsync",true,new Type[]{typeof(string),typeof(int)});y=a.
GetRuntimeProperty("Client");A=b.Invoke(new object[0]);}internal Task B(string a
,int b){return(Task)x.Invoke(A,new object[]{a,b});}}internal class aj{private
MethodInfo x;private MethodInfo y;public object Object{get;private set;}public
aj(int a){var b=a3.FindType("System.Net.Sockets.UdpClient");var c=a3.
GetConstructor(b,typeof(int));var d=a3.FindType("System.Net.IPEndPoint");x=b.
GetMethod("BeginReceive",true,new Type[]{typeof(AsyncCallback),typeof(object)});
y=b.GetMethod("EndReceive",true,new Type[]{typeof(IAsyncResult),d.MakeByRefType(
)});Object=c.Invoke(new object[]{a});}public void BeginReceive(AsyncCallback a,
object b){x.Invoke(Object,new object[]{a,b});}public byte[]EndReceive(
IAsyncResult a,ref object b){return(byte[])y.Invoke(Object,new object[]{a,b});}}
internal class __internal { public static readonly BindableProperty RegisterProperty = Runtime.RegisterProperty; } } 