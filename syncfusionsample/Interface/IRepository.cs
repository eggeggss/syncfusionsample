﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public interface IRepository<T>
    {
        T Get(int pk);
        IList<T> GetAll();

        void Insert(T obj);
        void InsertAll(IList<T> lists);

        void Delete(T obj);
        void DeleteAll();

        void Update(T obj);
        void UpdateAll(IList<T> lists);

    }
}
