﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public interface IActorRepository:IRepository<Actor>
    {
        Actor Get(String name);
    }
}
